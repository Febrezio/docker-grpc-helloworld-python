# syntax=docker/dockerfile:1

FROM ubuntu:20.04
WORKDIR /app
COPY . .
RUN apt-get update
RUN apt-get install -y --no-install-recommends python3 pip -y
RUN pip install -r requirements.txt
RUN python3 -m grpc_tools.protoc --proto_path=protos --python_out=. --pyi_out=. --grpc_python_out=. protos/helloworld.proto
CMD ["python3", "greeter_server.py"]
EXPOSE 50051