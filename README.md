# Docker Helloword Application

## Setup
### Install docker
1. Download the [latest docker installer](https://docs.docker.com/get-docker/).
2. Follow the [docker installation instructions](https://docs.docker.com/get-docker/).
### Build the image
1. Run `docker build -t docker-grpc-helloworld .`
### Start the application container
1. Run `docker run --publish 127.0.0.1:50051:50051 docker-grpc-helloworld`
### Test
1. Run `python greeter_client.py`